package com.doggy.webapp.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoggyWebbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoggyWebbappApplication.class, args);
	}
}
